const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const TodoSchema = new Schema({
  action: {
    type: String,
    required: [true, 'Yoou need to enter a task !']
  }
});

const Todo = mongoose.model('todo', TodoSchema);

module.exports = Todo;