const mongoose = require('mongoose');
const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const routes = require('./routes/api');

const API_PORT = 3001;
const app = express();

const dbRoute ='mongodb://claudine:azerty123@ds033841.mlab.com:33841/todoclaudine'

mongoose
  .connect(dbRoute, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
  .then(() => {
    console.log('Connected to DB!')
  })
  .catch(() => {
    console.log(' Unable to connect to DB...')
  })

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger('dev'));

app.use((err, req, res, next) => {
  console.log(err);
  next();
});

app.use('/api', routes);
app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));