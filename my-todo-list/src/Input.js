import React, { Component } from 'react';
import axios from 'axios';
import './Todo.css';


class Input extends Component {

  state = {
    action: ""
  }

  addTodo = () => {
    const task = {action: this.state.action}

    if(task.action && task.action.length > 0){
      axios.post('http://localhost:3001/api/todos', task)
        .then(res => {
          if(res.data){
            this.props.getTodos();
            this.setState({action: ""})
          }
        })
        .catch(err => console.log(err))
    }else {
      console.log('input field required')
    }
  }

  handleChange = (e) => {
    this.setState({
      action: e.target.value
    })
  }

  render() {
    let { action } = this.state;
    return (
      <div className="todo-input form-group">
        <input type="text" className="form-item form-control "placeholder="Add new task to the list" size="50" onChange={this.handleChange} value={action} />
        <button type="button" className=" form-item btn btn-light"onClick={this.addTodo}>add todo :-)</button>
      </div>
    )
  }
}

export default Input