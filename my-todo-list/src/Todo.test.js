import React from 'react';
import axios from 'axios';
import { shallow } from 'enzyme';
import TestUtils from 'react-dom/test-utils';
import Todo from './Todo';


jest.mock('axios', ()=> {
  const data = {
    todos: {
      id: 1,
      type: "learn react"
    },
  };
  return {
    get: jest.fn().mockReturnValue(Promise.resolve(data))
  }
})

describe('Component Todo', () => {
  const axiosMock = jest.spyOn(axios, 'get');
  const Todo = shallow(<Todo />);
  const TodoInstance = Todo.instance();
  
  beforeEach(() => {
    TodoInstance.componentDidMount();
  });
  test('it renders correctly', () => {
    expect(Todo).toMatchSnapshot();
  });
  test('Axios GET method is called when component is mounted', () => {
    expect(axiosMock).toBeCalled();
    expect(axiosMock).toHaveReturned();
  });
})
