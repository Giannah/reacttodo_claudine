import React from 'react';
import './Todo.css';

const TodoList = ({ todos, deleteTodo }) => {

  return (
    <ul>
      {
        todos &&
          todos.length > 0 ?
            (
              todos.map(todo => {
                return (
                  <div className="todo alert alert-primary" key={todo._id} ><span className="todo-item">{todo.action}</span><button type="button" className="btn btn-light"onClick={() => deleteTodo(todo._id)}>Remove !</button></div>
                )
              })
            )
            :
            (
              <li>Nothing to do yet !</li>
            )
      }
    </ul>
  )
}

export default TodoList;